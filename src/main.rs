use gameoflife::*;
use std::io::stdout;
use std::io::Write;
use termion::raw::IntoRawMode;

fn main() -> Result<()> {
    let (x, y) = termion::terminal_size()?;
    let (x, y) = (x as usize / 2, y as usize);

    let mut game = Game::new(x, y - 1).with_speed(10);
    game.randomize(2);

    let screen = &mut stdout().into_raw_mode()?;
    game.run(screen)?;
    screen.flush()?;
    println!("{}", game.statline());
    Ok(())
}

