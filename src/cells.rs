use crate::Game;
use std::collections::HashMap;
use std::fmt;
use std::ops::Add;

#[allow(unused)]
pub const GLIDER: &[(usize, usize)] = &[
    (1,3),
    (2,1),
    (2,3),
    (3,3),
    (3,2),
];

#[allow(unused)]
pub const OSCILLATOR: &[(usize, usize)] = &[
    (2,0),
    (1,1),
    (2,1),
    (3,1),
    (0,2),
    (1,2),
    (2,2),
    (3,2),
    (4,2),
    (0,9),
    (1,9),
    (2,9),
    (3,9),
    (4,9),
    (1,10),
    (2,10),
    (3,10),
    (2,11),
];

pub struct Board {
    pub(crate) width: usize,
    pub(crate) height: usize,
}

pub use Cell::*;
/// The state of a cell. It can be `Alive` or `Dead`
#[derive(Debug, PartialEq, Eq)]
pub enum Cell {
    /// It's alive
    Alive,
    /// It's dead
    Dead,
}

impl Default for Cell {
    fn default() -> Self {
        Dead
    }
}

impl<'a> Default for &'a Cell {
    fn default() -> Self {
        &Dead
    }
}

impl Cell {
    pub fn is_alive(&self) -> bool {
        self == &Alive
    }

    pub fn flip(&mut self) -> bool {
        match self {
            Alive => {*self = Dead; false},
            Dead => {*self = Alive; true},
        }
    }
}

impl fmt::Display for Cell {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}",
            match self {
                Alive => '⬛',
                Dead  => '⬜',
            }
        )
    }
}

pub struct Cells {
    alive: Vec<Point>,
}

impl Cells {
    pub fn cells(&self) -> HashMap<Point, Cell> {
        let mut cells = HashMap::new();

        let min_x = self.min_x();
        let max_x = self.max_x();
        let min_y = self.min_y();
        let max_y = self.max_y();

        if let (Some(min_x), Some(max_x), Some(min_y), Some(max_y)) = (min_x, max_x, min_y, max_y) {
            for y in min_y..=max_y {
                for x in min_x..=max_x {
                    let point = Point::new(x, y);
                    if self.alive.contains(&point) {
                        cells.insert(point, Alive);
                    } else {
                        cells.insert(point, Dead);
                    }
                }
            }
        }

        cells
    }

    pub fn normalize(&self) -> Self {
        let min_x = self.min_x().unwrap_or(0);
        let min_y = self.min_y().unwrap_or(0);
        Cells {
            alive: self
                .alive
                .iter()
                .map(|Point {x, y}| Point::new(x-min_x, y-min_y))
                .collect()
        }
    }

    pub fn min_x(&self) -> Option<usize> {
        Some(self.alive.iter().min_by(|pointa, pointb| pointa.x.cmp(&pointb.x))?.x)
    }

    pub fn max_x(&self) -> Option<usize> {
        Some(self.alive.iter().max_by(|pointa, pointb| pointa.x.cmp(&pointb.x))?.x)
    }

    pub fn min_y(&self) -> Option<usize> {
        Some(self.alive.iter().min_by(|pointa, pointb| pointa.y.cmp(&pointb.y))?.y)
    }

    pub fn max_y(&self) -> Option<usize> {
        Some(self.alive.iter().max_by(|pointa, pointb| pointa.y.cmp(&pointb.y))?.y)
    }
}

impl<'a> From<&'a [(usize, usize)]> for Cells {
    fn from(points: &'a [(usize, usize)]) -> Self {
        Cells {
            alive: points.into_iter().map(|(x, y)| Point::new(*x, *y)).collect()
        }
    }
}

impl Add<&Point> for Cells {
    type Output = Self;
    fn add(self, rhs: &Point) -> Self::Output {
        Cells {
            alive: self
                .alive
                .iter()
                .map(|Point {x, y}| Point::new(x + rhs.x, y + rhs.y))
                .collect()
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Point {
    pub x: usize,
    pub y: usize,
}

impl Point {
    pub const fn new(x: usize, y: usize) -> Self {
        Point {x, y}
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({},{})", self.x, self.y)
    }
}

impl From<(usize, usize)> for Point {
    fn from(point: (usize, usize)) -> Self {
        Point::new(point.0, point.1)
    }
}

impl From<&(usize, usize)> for Point {
    fn from(point: &(usize, usize)) -> Self {
        Point::from(*point)
    }
}

pub struct Points {
    points: Vec<Point>,
    index: usize,
}

impl Iterator for Points {
    type Item = Point;
    fn next(&mut self) -> Option<Self::Item> {
        if self.index < self.points.len() {
            self.index += 1;
            Some(self.points[self.index - 1])
        } else {
            None
        }
    }
}

impl<'a> From<&'a Game> for Points {
    fn from(game: &'a Game) -> Self {
        Points {
            points: {
                (0..game.board.height).into_iter()
                    .flat_map(|y| (0..game.board.width).into_iter().map(move |x| Point {x, y}))
                    .collect()
            },
            index: 0,
        }
    }
}
