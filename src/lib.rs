use rand::prelude::*;
use std::cmp::Ordering;
use std::fmt;
use std::io::Write;
use std::ops::{Index, IndexMut};
use termion::{async_stdin, clear, cursor, event::Key, input::TermRead, screen::AlternateScreen};

pub mod cells;
pub use cells::*;

pub type BoxErr = Box<dyn std::error::Error>;
pub type Result<T> = std::result::Result<T, BoxErr>;

pub struct Game {
    board: Board,
    grid: Vec<Cell>,
    state: State,
    stalled: bool,
    generations: usize,
    max: usize,
    min: usize,
    growth: Ordering,
    alive: usize,
    speed: u64,
    insertion: Point,
}

impl fmt::Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (x, cell) in self.cells().enumerate() {
            if x % self.width() == 0 && x != 0 {
                write!(f, "\r\n")?;
            }
            if self.point_from_index(x) == self.insertion {
                write!(f, "\x1B[31m{}\x1B[0m", cell)?;
            } else {
                write!(f, "{}", cell)?;
            }
        }
        write!(f, "{}", self.statline())
    }
}

impl<'a> Index<&'a Point> for Game {
    type Output = Cell;
    fn index(&self, index: &'a Point) -> &Self::Output {
        let Point {x, y} = index;
        let index = grid_index(*x, *y, self.width(), self.height());
        &self.grid[index]
    }
}

impl<'a> IndexMut<&'a Point> for Game {
    fn index_mut(&mut self, index: &'a Point) -> &mut Self::Output {
        let Point {x, y} = index;
        let index = grid_index(*x,*y, self.width(), self.height());
        &mut self.grid[index]
    }
}

impl Index<Point> for Game {
    type Output = Cell;
    fn index(&self, index: Point) -> &Self::Output {
        &self[&index]
    }
}

impl IndexMut<Point> for Game {
    fn index_mut(&mut self, index: Point) -> &mut Self::Output {
        let Point {x, y} = index;
        let index = grid_index(x,y, self.width(), self.height());
        &mut self.grid[index]
    }
}

impl<'a> Index<&'a (usize, usize)> for Game {
    type Output = Cell;
    fn index(&self, index: &'a (usize, usize)) -> &Self::Output {
        let (x, y) = index;
        let index = grid_index(*x, *y, self.width(), self.height());
        &self.grid[index]
    }
}

impl<'a> IndexMut<&'a (usize, usize)> for Game {
    fn index_mut(&mut self, index: &'a (usize, usize)) -> &mut Self::Output {
        let (x, y) = index;
        let index = grid_index(*x, *y, self.width(), self.height());
        &mut self.grid[index]
    }
}

fn grid_index(x: usize, y: usize, width: usize, height: usize) -> usize {
    ((y % height) * width) + (x % width)
}

#[test]
fn index() {
    let (w, h) = (3, 4);
    assert_eq!(grid_index(0,0, w, h), 0);
    assert_eq!(grid_index(1,0, w, h), 1);
    assert_eq!(grid_index(2,0, w, h), 2);
    assert_eq!(grid_index(0,1, w, h), 3);
    assert_eq!(grid_index(1,1, w, h), 4);
    assert_eq!(grid_index(2,1, w, h), 5);
    assert_eq!(grid_index(0,2, w, h), 6);
    assert_eq!(grid_index(1,2, w, h), 7);
    assert_eq!(grid_index(2,2, w, h), 8);
    assert_eq!(grid_index(0,3, w, h), 9);
    assert_eq!(grid_index(1,3, w, h), 10);
    assert_eq!(grid_index(2,3, w, h), 11);

    // Overflow bottom
    assert_eq!(grid_index(0,4, w, h), 0);
    assert_eq!(grid_index(1,4, w, h), 1);
    assert_eq!(grid_index(2,4, w, h), 2);
    assert_eq!(grid_index(3,4, w, h), 0);

    // Overflow right
    assert_eq!(grid_index(3,0, w, h), 0);
    assert_eq!(grid_index(3,1, w, h), 3);
    assert_eq!(grid_index(3,2, w, h), 6);
    assert_eq!(grid_index(3,3, w, h), 9);
}

#[test]
fn delay() {
    let mut game = Game::new(0,0).with_speed(10);
    assert_eq!(game.delay(), 100);

    game.set_speed(5);
    assert_eq!(game.delay(), 200);


    game.set_speed(1);
    assert_eq!(game.delay(), 1000);
}

#[test]
fn point_from_index() {
    let game = Game::new(4, 6);
    assert_eq!(game.grid.len(), 24);

    // In-bounds
    assert_eq!(game.point_from_index(0),  Point::new(0, 0), "index=0");
    assert_eq!(game.point_from_index(1),  Point::new(1, 0), "index=1");
    assert_eq!(game.point_from_index(2),  Point::new(2, 0), "index=2");
    assert_eq!(game.point_from_index(5),  Point::new(1, 1), "index=5");
    assert_eq!(game.point_from_index(6),  Point::new(2, 1), "index=6");
    assert_eq!(game.point_from_index(14), Point::new(2, 3), "index=14");
    assert_eq!(game.point_from_index(23), Point::new(3, 5), "index=23");

    // Overflow
    assert_eq!(game.point_from_index(24), Point::new(0, 0), "index=24");
    assert_eq!(game.point_from_index(25), Point::new(1, 0), "index=25");
    assert_eq!(game.point_from_index(26), Point::new(2, 0), "index=26");
    assert_eq!(game.point_from_index(27), Point::new(3, 0), "index=27");
    assert_eq!(game.point_from_index(30), Point::new(2, 1), "index=30");
    assert_eq!(game.point_from_index(38), Point::new(2, 3), "index=38");
    assert_eq!(game.point_from_index(47), Point::new(3, 5), "index=47");
}

impl Game {
    pub fn new(width: usize, height: usize) -> Self {
        Game {
            board: Board { width, height },
            grid: (0..(width * height)).map(|_| Dead).collect(),
            state: Running,
            stalled: false,
            generations: 0,
            max: 0,
            min: 0,
            growth: Ordering::Equal,
            alive: 0,
            speed: 10,
            insertion: Point::new(width / 2, height / 2),
        }
    }

    pub fn statline(&self) -> String {
        let color = match self.growth {
            Ordering::Less => "\x1B[32m",
            Ordering::Greater => "\x1B[31m",
            Ordering::Equal => "",
        };

        format!("\r\n{clear}gen: {gen} | cells: {color}{alive}\x1B[0m | max: {max} | min: {min} | {width}x{height} | {speed}gen/s | {insertion}",
                clear = clear::CurrentLine,
                gen = self.generations,
                color = color,
                alive = self.alive,
                max = self.max,
                min = self.min,
                width = self.width(),
                height = self.height(),
                speed = self.speed,
                insertion = self.insertion,
            )
    }

    fn point_from_index(&self, index: usize) -> Point {
        let len = self.grid.len();
        let width = self.width();
        let index = index % len;
        Point {
            x: index % width,
            y: index % len / width,
        }
    }

    pub fn with_speed(mut self, gps: u64) -> Self {
        self.set_speed(gps);
        self
    }

    pub fn set_speed(&mut self, gps: u64) {
        self.speed = gps;
    }

    pub fn speed_up(&mut self) {
        self.speed += 1;
    }

    pub fn super_speed_up(&mut self) {
        self.speed *= 10;
    }

    pub fn slow_down(&mut self) {
        if self.speed > 1 {
            self.speed -= 1;
        }
    }

    pub fn super_slow_down(&mut self) {
        if self.speed > 1 {
            self.speed /= 10;
        }
    }

    pub fn delay(&self) -> u64 {
        1000 / self.speed
    }

    fn width(&self) -> usize {
        self.board.width
    }

    fn height(&self) -> usize {
        self.board.height
    }

    pub fn is_stalled(&self) -> bool {
        self.stalled
    }

    pub fn set_cell(&mut self, point: &Point, cell: Cell) {
        match (self.index(point), &cell) {
            (&Alive, &Dead) => self.alive -= 1,
            (&Dead, &Alive) => self.alive += 1,
            _ => (),
        }

        self[point] = cell;
    }

    pub fn initialize(&mut self, cells: Cells) {
        self.reset();
        self.insert(cells);
        self.max = self.alive;
        self.min = self.alive;
    }

    pub fn initialize_at(&mut self, cells: Cells, point: &Point) {
        self.initialize(cells.normalize() + point)
    }

    pub fn initialize_at_center(&mut self, cells: Cells) {
        self.initialize_at(cells, &self.center())
    }

    pub fn insert(&mut self, cells: Cells) {
        for (point, cell) in cells.cells() {
            self.set_cell(&point, cell);
        }
        self.alive = self.alive_count();
    }

    pub fn insert_at(&mut self, cells: Cells, point: &Point) {
        self.insert(cells.normalize() + point)
    }

    pub fn insert_center(&mut self, cells: Cells) {
        self.insert_at(cells, &self.center())
    }

    pub fn increment_insertion_x(&mut self, n: usize) {
        for _ in 0..n {
            if self.insertion.x == self.last_x() {
                self.insertion.x = 0;
            } else {
                self.insertion.x += 1;
            }
        }
    }

    pub fn decrement_insertion_x(&mut self, n: usize) {
        for _ in 0..n {
            if self.insertion.x == 0 {
                self.insertion.x = self.width() - 1;
            } else {
                self.insertion.x -= 1;
            }
        }
    }

    pub fn increment_insertion_y(&mut self, n: usize) {
        for _ in 0..n {
            if self.insertion.y == self.last_y() {
                self.insertion.y = 0;
            } else {
                self.insertion.y += 1;
            }
        }
    }

    pub fn decrement_insertion_y(&mut self, n: usize) {
        for _ in 0..n {
            if self.insertion.y == 0 {
                self.insertion.y = self.height() - 1;
            } else {
                self.insertion.y -= 1;
            }
        }
    }

    pub fn flip(&mut self, index: &Point) {
        if self[index].flip() {
            self.alive += 1;
        } else {
            self.alive -= 1;
        }
    }

    pub fn reset(&mut self) {
        self.generations = 0;
        self.alive = 0;
        self.max = 0;
        self.min = 0;
        self.state = Running;
        self.cells_mut().for_each(|cell| *cell = Dead)
    }

    pub fn all_dead(&self) -> bool {
        self.cells().all(|cell| cell == &Dead)
    }

    pub fn points(&self) -> Points {
        self.into()
    }

    pub fn cells(&self) -> std::slice::Iter<Cell> {
        self.grid.iter()
    }

    pub fn cells_mut(&mut self) -> std::slice::IterMut<Cell> {
        self.grid.iter_mut()
    }

    pub fn randomize(&mut self, modulus: usize) {
        self.reset();
        let mut rng = thread_rng();
        for point in self.points() {
            if rng.gen::<usize>() % modulus == 0 {
                self.flip(&point);
            }
        }
    }

    pub fn run<W: Write>(&mut self, screen: &mut W) -> Result<()> {
        let screen = &mut AlternateScreen::from(screen);
        let stdin = &mut async_stdin();
        write!(
            screen,
            "{}{}{}",
            cursor::Hide,
            cursor::Goto(1, 1),
            clear::All
        )?;

        while !self.is_stalled() {
            let insertion = self.insertion;
            self.redraw(screen)?;
            self.step();
            for key in stdin.keys() {
                match key {
                    Ok(key) => match key {
                        Key::Char('q') | Key::Ctrl('c') | Key::Esc => {
                            self.stalled = true;
                        }
                        Key::Char('r') => self.randomize(5),
                        Key::Char('g') => self.insert_at(cells::GLIDER.into(), &insertion),
                        Key::Char('G') => self.initialize_at_center(cells::GLIDER.into()),
                        Key::Char('S') => self.set_speed(10000),
                        Key::Char('s') => self.set_speed(1),
                        Key::Char(' ') => self.toggle_state(),
                        Key::Char('+') => self.speed_up(),
                        Key::Char('-') => self.slow_down(),
                        Key::Char('*') => self.super_speed_up(),
                        Key::Char('/') => self.super_slow_down(),
                        Key::Char('2') => self.randomize(2),
                        Key::Char('3') => self.randomize(3),
                        Key::Char('4') => self.randomize(4),
                        Key::Char('5') => self.randomize(5),
                        Key::Char('6') => self.randomize(6),
                        Key::Char('7') => self.randomize(7),
                        Key::Char('8') => self.randomize(8),
                        Key::Char('9') => self.randomize(9),
                        Key::Char('n') => self.single_step(screen)?,
                        Key::Char('l') | Key::Right => self.increment_insertion_x(1),
                        Key::Char('L') => self.increment_insertion_x(5),
                        Key::Char('h') | Key::Left => self.decrement_insertion_x(1),
                        Key::Char('H') => self.decrement_insertion_x(5),
                        Key::Char('j') | Key::Down => self.increment_insertion_y(1),
                        Key::Char('J') => self.increment_insertion_y(5),
                        Key::Char('k') | Key::Up => self.decrement_insertion_y(1),
                        Key::Char('K') => self.decrement_insertion_y(5),
                        _ => (),
                    },
                    _ => (),
                }
            }
            screen.flush()?;
            sleep(self.delay());
        }

        write!(screen, "{}", cursor::Show)?;
        Ok(())
    }

    pub fn redraw<W: Write>(&self, screen: &mut W) -> Result<()> {
        match self.state {
            Paused => Ok(()),
            Running => {
                write!(screen, "{}{}", cursor::Goto(1, 1), self)?;
                Ok(())
            }
        }
    }

    pub fn step(&mut self) {
        if self.paused() {
            return;
        }

        let last_alive = self.alive;

        let mut flips = Vec::new();

        for point in self.points() {
            let neighbors = self.surround_count(&point);
            match self[&point] {
                Dead => {
                    if neighbors == 3 {
                        flips.push(point);
                    }
                }
                Alive => {
                    if neighbors != 2 && neighbors != 3 {
                        flips.push(point);
                    }
                }
            }
        }

        if flips.is_empty() {
            self.stalled = true;
        }

        for index in flips {
            self.flip(&index);
        }

        if self.generations == 0 {
            self.max = self.alive;
            self.min = self.alive;
        } else if self.alive > self.max {
            self.max = self.alive;
        } else if self.alive < self.min {
            self.min = self.alive;
        }

        self.growth = self.alive.cmp(&last_alive);

        self.generations += 1;
    }

    pub fn single_step<W: Write>(&mut self, screen: &mut W) -> Result<()> {
        self.state = Running;
        self.step();
        self.redraw(screen)?;
        self.state = Paused;
        Ok(())
    }

    fn alive_count(&self) -> usize {
        self.cells().filter(|cell| cell.is_alive()).count()
    }

    pub fn surround_count(&self, index: &Point) -> usize {
        self.surround(index)
            .iter()
            .filter(|cell| cell.is_alive())
            .count()
    }

    pub fn surround_points(&self, index: &Point) -> [Point; 8] {
        [
            self.left_top(&index),
            self.middle_top(&index),
            self.right_top(&index),
            self.left_middle(&index),
            self.right_middle(&index),
            self.left_bottom(&index),
            self.middle_bottom(&index),
            self.right_bottom(&index),
        ]
    }

    pub fn surround(&self, index: &Point) -> [&Cell; 8] {
        let [left_top, middle_top, right_top, left_middle, right_middle, left_bottom, middle_bottom, right_bottom] =
            self.surround_points(index);

        [
            &self[left_top],
            &self[middle_top],
            &self[right_top],
            &self[left_middle],
            &self[right_middle],
            &self[left_bottom],
            &self[middle_bottom],
            &self[right_bottom],
        ]
    }

    fn left_top(&self, index: &Point) -> Point {
        match (index.x, index.y) {
            (0, 0) => self.last_point(),
            (0, y) => Point::new(self.last_x(), y - 1),
            (x, 0) => Point::new(x - 1, self.last_y()),
            (x, y) => Point::new(x - 1, y - 1),
        }
    }

    fn middle_top(&self, index: &Point) -> Point {
        match (index.x, index.y) {
            (x, 0) => Point::new(x, self.last_y()),
            (x, y) => Point::new(x, y - 1),
        }
    }

    fn right_top(&self, index: &Point) -> Point {
        match (index.x, index.y) {
            (x, 0) => Point::new(x + 1, self.last_y()),
            (x, y) => Point::new(x + 1, y - 1),
        }
    }

    fn left_middle(&self, index: &Point) -> Point {
        match (index.x, index.y) {
            (0, y) => Point::new(self.last_x(), y),
            (x, y) => Point::new(x - 1, y),
        }
    }

    fn right_middle(&self, index: &Point) -> Point {
        Point::new(index.x + 1, index.y)
    }

    fn left_bottom(&self, index: &Point) -> Point {
        match (index.x, index.y) {
            (0, y) => Point::new(self.last_x(), y + 1),
            (x, y) => Point::new(x - 1, y + 1),
        }
    }

    fn middle_bottom(&self, index: &Point) -> Point {
        Point::new(index.x, index.y + 1)
    }

    fn right_bottom(&self, index: &Point) -> Point {
        Point::new(index.x + 1, index.y + 1)
    }

    fn last_point(&self) -> Point {
        Point::new(self.last_x(), self.last_y())
    }

    fn last_x(&self) -> usize {
        self.width() - 1
    }

    fn last_y(&self) -> usize {
        self.height() - 1
    }

    pub fn center(&self) -> Point {
        Point::new(self.width() / 2, self.height() / 2)
    }

    pub fn toggle_state(&mut self) {
        match self.state {
            Running => self.state = Paused,
            Paused => self.state = Running,
        }
    }

    pub fn paused(&self) -> bool {
        self.state == Paused
    }
}

use State::*;
#[derive(PartialEq, Eq)]
enum State {
    Running,
    Paused,
}

fn sleep(ms: u64) {
    std::thread::sleep(std::time::Duration::from_millis(ms));
}

#[test]
fn glider() {
    let mut game = Game::new(10, 10);

    // Glider
    game.initialize(cells::GLIDER.into());

    assert_eq!(game.alive, game.alive_count());

    println!("{}", &game);

    assert_eq!(game.surround_count(&(0,0).into()), 0);
    assert_eq!(game.surround_count(&(2,1).into()), 1);
    assert_eq!(game.surround_count(&(2,2).into()), 5);
    assert_eq!(game.surround_count(&(3,3).into()), 2);
    assert_eq!(game.surround_count(&(3,2).into()), 3);
    assert_eq!(game.surround_count(&(2,4).into()), 3);

    game.step();
    assert_eq!(game.alive, game.alive_count());

    assert_eq!(game.surround_count(&(0,0).into()), 0);
    assert_eq!(game.surround_count(&(1,2).into()), 1);
    assert_eq!(game.surround_count(&(3,2).into()), 2);
    assert_eq!(game.surround_count(&(2,3).into()), 4);
    assert_eq!(game.surround_count(&(3,3).into()), 3);
    assert_eq!(game.surround_count(&(2,4).into()), 2);

    println!("{}", &game);

    for _ in 0..26 {
        game.step();
        assert_eq!(game.alive, game.alive_count());
        println!("{}", &game);
    }

    assert!(!game.stalled);
    game.step();
    assert!(!game.stalled);
}
